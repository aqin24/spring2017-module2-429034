<!doctype html>
<html>
<head><title>Answer</title></head>
<body>
<?php
$firstnumber = $_POST["firstnumber"];
$secondnumber = $_POST["secondnumber"];
$sum = $firstnumber + $secondnumber;
$difference = $firstnumber - $secondnumber;
$product = $firstnumber * $secondnumber;
$quotient = 0;
if($secondnumber == 0){
$quotient = null;}
else{
    $quotient = $firstnumber / $secondnumber;
}

$operator = $_POST["operator"];
switch($operator)
{
    case "addition":
        if(is_numeric($firstnumber) == False || is_numeric($secondnumber) == False){
            echo "Please enter a number";
            break;
        }
        else{
        echo "Answer: " .$sum;
        break;
        }
    case "subtraction":
        if(is_numeric($firstnumber) == False || is_numeric($secondnumber) == False){
            echo "Please enter a number";
            break;
        }
        else{
        echo "Answer: " .$difference;
        break;
        }
    case "multiplication":
        if(is_numeric($firstnumber) == False || is_numeric($secondnumber) == False){
            echo "Please enter a number";
            break;
        }
        else{
        echo "Answer: " .$product;
        break;
        }
    case "division":
        if($secondnumber == 0){
            echo "You can't divide by zero";
            break;
        }
        else if(is_numeric($firstnumber) == False || is_numeric($secondnumber) == False){
            echo "Please enter a number";
            break;
        }
        else{
            echo "Answer:" .$quotient;
            break;
        }
    default:
        echo "Please select an operator";
        break;

}
?>
</body>
</html>